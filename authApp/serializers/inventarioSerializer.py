from authApp.models.Inventario import Inventario
from rest_framework import serializers
class InventarioSerializer(serializers.ModelSerializer):
 class Meta:
    model = Inventario
    fields = ['asignacion', 'fecha', 'tipo', 'estado']
