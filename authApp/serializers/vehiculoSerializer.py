from authApp.models.Vehiculo import Vehiculo
from rest_framework import serializers

class VehiculoSerializer(serializers.ModelSerializer):
  class Meta:
     model = Vehiculo
     fields = ['placa', 'tipo', 'modelo']
