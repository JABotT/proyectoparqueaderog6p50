from authApp.models.Administrador import Administrador
from authApp.models.Inventario import Inventario
from authApp.models.usuario import Usuario
from authApp.serializers.inventarioSerializer import InventarioSerializer
from authApp.serializers.usuarioSerializer import UsuarioSerializer
from rest_framework import serializers



class AdministradorSerializer(serializers.ModelSerializer):
   inventario = InventarioSerializer()
   usuario = UsuarioSerializer()

   class Meta:
      model = Administrador
      fields = ['id''email', 'password', 'inventario', 'usuario']

   def create(self, validated_data):
       inventarioData = validated_data.pop('inventario')
       administradorInstance = Administrador.objects.create(**validated_data)
       Inventario.objects.create(administrador=administradorInstance, **accountData)
       return administradorInstance

   def create(self, validated_data):
       usuarioData = validated_data.pop('usuario')
       administradorInstance = Administrador.objects.create(**validated_data)
       Usuario.objects.create(administrador=administradorInstance, **accountData)
       return administradorInstance


   def to_representation(self, obj):
       administrador = Administrador.objects.get(id=obj.id)
       inventario = inventario.objects.get(administrador=obj.espacio)
       usuario = usuario.objects.get(administrador=obj.cedula)       
       return {
                    'id': administrador.id, 
                    'email': administrador.email,
                    'inventario': {
                        'espacio': inventario.espacio,
                        'asignacion': inventario.asignacion,
                        'fecha': inventario.fecha,
                        'tipo': inventario.tipo,
                        'estado': inventario.estado,                        
                     },
                    'usuario': {
                       'cedula': usuario.cedula,
                       'celular': usuario.celular,
                       'nombre': usuario.nombre,
                       'apellido': usuario.apellido,
                       'email': usuario.email,
                       'direccion':usuario.direccion
                     }
                                       
               }