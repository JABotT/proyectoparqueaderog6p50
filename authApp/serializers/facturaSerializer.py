from authApp.models.Factura import Factura
from rest_framework import serializers

class FacturaSerializer(serializers.ModelSerializer):
   class Meta:
       model = Factura
       fields = ['tarifa', 'total', 'tiempo']