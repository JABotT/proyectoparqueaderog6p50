from authApp.models.usuario import Usuario
from authApp.serializers.inventarioSerializer import InventarioSerializer
from authApp.serializers.facturaSerializer import FacturaSerializer
from authApp.serializers.vehiculoSerializer import VehiculoSerializer 
from authApp.models.Inventario import Inventario
from authApp.models.Factura import Factura
from authApp.models.Vehiculo import Vehiculo

from rest_framework import serializers
class UsuarioSerializer(serializers.ModelSerializer):

   inventario = InventarioSerializer()
   factura = FacturaSerializer()
   vehiculo = VehiculoSerializer()   
   
   class Meta:
       model = Usuario
       fields = ['cedula', 'celular', 'nombre', 'apellido', 'email', 'direccion', 'inventario', 'factura', 'vehiculo']


   def create(self, validated_data):
       inventarioData = validated_data.pop('inventario')
       usuarioInstance = Usuario.objects.create(**validated_data)
       Inventario.objects.create(usuario=usuarioInstance, **inventarioData)
       return usuarioInstance

   def create(self, validated_data):
       facturaData = validated_data.pop('factura')
       usuarioInstance = Usuario.objects.create(**validated_data)
       Factura.objects.create(usuario=usuarioInstance, **facturaData)
       return usuarioInstance

   def create(self, validated_data):
       vehiculoData = validated_data.pop('vehiculo')
       usuarioInstance = Usuario.objects.create(**validated_data)
       Vehiculo.objects.create(usuario=usuarioInstance, **vehiculoData)
       return usuarioInstance

   def to_representation(self, obj):
       usuario = Usuario.objects.get(id=obj.cedula)
       inventario = Inventario.objects.get(usuario=obj.espacio)
       factura = Factura.objects.get(usuario=obj.id)
       vehiculo = Vehiculo.objects.get(usuario=obj.id)      
       return {
                    'cedula': usuario.cedula,
                    'celular': usuario.celular, 
                    'nombre': usuario.nombre,
                    'apellido': usuario.apellido,
                    'email': usuario.email,
                    'direccion': usuario.direccion,
                    'inventario': {
                        'espacio': inventario.espacio,
                        'asignacion': inventario.asignacion,
                        'fecha': inventario.fecha,
                        'tipo': inventario.tipo,
                        'estado': inventario.estado,                        
                     },
                    'factura': {
                       'id': factura.id,
                       'tarifa': factura.tarifa,
                       'total': factura.total,
                       'tiempo': factura.tiempo,
                     },
                     'vehiculo': {
                        'id': vehiculo.id,
                        'placa': vehiculo.placa,
                        'tipo': vehiculo.tipo,
                        'modelo': vehiculo.modelo
                     }
                                       
               }