from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from authApp.models import Administrador
from authApp.serializers import administradorSerializer
from rest_framework import status, views
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class AdministradorCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = administradorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        tokenData = {"email":request.data["email"],
        "password":request.data["password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)