from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from authApp.models import Vehiculo
from authApp.serializers import VehiculoSerializer


@api_view(['GET','POST'])
def vehiculo_api_view(request):

    if request.method == 'GET':
        vehiculo = Vehiculo.objects.all()
        vehiculo_serializer = VehiculoSerializer(vehiculo,many=True)
        return Response(vehiculo_serializer.data)

    elif request.method == 'POST':
        vehiculo_serializer = VehiculoSerializer(data = request.data)
        if vehiculo_serializer.is_valid():
            vehiculo_serializer.save()
            return Response(vehiculo_serializer.data)
        return Response(vehiculo_serializer.errors)

@api_view(['GET','PUT','DELETE'])
def vehiculo_detail_view(request,pk=None):

    if request.method == 'GET':
        vehiculo = Vehiculo.objects.filter(id = pk).first()
        vehiculo_serializer = VehiculoSerializer(vehiculo)
        return Response(vehiculo_serializer.data)
    
    elif request.method == 'PUT':
        vehiculo = Vehiculo.objects.filter(id = pk).first()
        vehiculo_serializer = VehiculoSerializer(data = request.data)
        if vehiculo_serializer.is_valid():
            vehiculo_serializer.save()
            return Response(vehiculo_serializer.data)
        return Response(vehiculo_serializer.errors)
    
    elif request.method == 'DELETE':
        vehiculo = Vehiculo.objects.filter(id = pk).first()
        vehiculo.delete()
        return Response("Eliminado..")