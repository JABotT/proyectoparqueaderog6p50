from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from authApp.models import Inventario
from authApp.serializers import InventarioSerializer


@api_view(['GET','POST'])
def inventario_api_view(request):

    if request.method == 'GET':
        inventario = Inventario.objects.all()
        inventario_serializer = InventarioSerializer(inventario,many=True)
        return Response(inventario_serializer.data)

    elif request.method == 'POST':
        inventario_serializer = InventarioSerializer(data = request.data)
        if inventario_serializer.is_valid():
            inventario_serializer.save()
            return Response(inventario_serializer.data)
        return Response(inventario_serializer.errors)

@api_view(['GET','PUT','DELETE'])
def inventario_detail_view(request,pk=None):

    if request.method == 'GET':
        inventario = Inventario.objects.filter(espacio = pk).first()
        inventario_serializer = InventarioSerializer(inventario)
        return Response(inventario_serializer.data)
    
    elif request.method == 'PUT':
        inventario = Inventario.objects.filter(espacio = pk).first()
        inventario_serializer = InventarioSerializer(data = request.data)
        if inventario_serializer.is_valid():
            inventario_serializer.save()
            return Response(inventario_serializer.data)
        return Response(inventario_serializer.errors)
    
    elif request.method == 'DELETE':
        inventario = Inventario.objects.filter(espacio = pk).first()
        inventario.delete()
        return Response("Eliminado..")