from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from authApp.models import Usuario
from authApp.serializers import UsuarioSerializer


@api_view(['GET','POST'])
def usuario_api_view(request):

    if request.method == 'GET':
        usuario = Usuario.objects.all()
        usuario_serializer = UsuarioSerializer(usuario,many=True)
        return Response(usuario_serializer.data)

    elif request.method == 'POST':
        usuario_serializer = UsuarioSerializer(data = request.data)
        if usuario_serializer.is_valid():
            usuario_serializer.save()
            return Response(usuario_serializer.data)
        return Response(usuario_serializer.errors)

@api_view(['GET','PUT','DELETE'])
def usuario_detail_view(request,pk=None):

    if request.method == 'GET':
        usuario = Usuario.objects.filter(cedula = pk).first()
        usuario_serializer = UsuarioSerializer(usuario)
        return Response(usuario_serializer.data)
    
    elif request.method == 'PUT':
        usuario = Usuario.objects.filter(cedula = pk).first()
        usuario_serializer = UsuarioSerializer(data = request.data)
        if usuario_serializer.is_valid():
            usuario_serializer.save()
            return Response(usuario_serializer.data)
        return Response(usuario_serializer.errors)
    
    elif request.method == 'DELETE':
        usuario = Usuario.objects.filter(cedula = pk).first()
        usuario.delete()
        return Response("Eliminado..")