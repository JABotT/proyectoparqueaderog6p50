from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from authApp.models import Factura
from authApp.serializers import FacturaSerializer, facturaSerializer


@api_view(['GET','POST'])
def factura_api_view(request):

    if request.method == 'GET':
        factura = Factura.objects.all()
        factura_serializer = FacturaSerializer(factura,many=True)
        return Response(factura_serializer.data)

    elif request.method == 'POST':
        factura_serializer = FacturaSerializer(data = request.data)
        if factura_serializer.is_valid():
            factura_serializer.save()
            return Response(factura_serializer.data)
        return Response(factura_serializer.errors)

@api_view(['GET','PUT','DELETE'])
def factura_detail_view(request,pk=None):

    if request.method == 'GET':
        factura = Factura.objects.filter(id = pk).first()
        factura_serializer = FacturaSerializer(factura)
        return Response(factura_serializer.data)
    
    elif request.method == 'PUT':
        factura = Factura.objects.filter(id = pk).first()
        factura_serializer = FacturaSerializer(data = request.data)
        if factura_serializer.is_valid():
            factura_serializer.save()
            return Response(factura_serializer.data)
        return Response(factura_serializer.errors)
    
    elif request.method == 'DELETE':
        factura = Factura.objects.filter(id = pk).first()
        factura.delete()
        return Response("Eliminado..")