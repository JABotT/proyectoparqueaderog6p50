from django.db import models


class Administrador(models.Model):
    id = models.IntegerField(primary_key=True)
    email = models.EmailField(max_length = 50)
    password = models.CharField(max_length = 50)
    