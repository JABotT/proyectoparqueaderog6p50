from django.db import models
from .Administrador import Administrador 

class Usuario(models.Model):
    cedula = models.IntegerField(primary_key=True)
    id = models.ForeignKey(Administrador, on_delete=models.CASCADE)
    celular = models.CharField(max_length = 15, unique=True)
    nombre = models.CharField(max_length = 20)
    apellido = models.CharField( max_length = 20)
    email = models.EmailField(max_length = 50)
    direccion = models.CharField(max_length = 50)

    
