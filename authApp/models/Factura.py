from django.db import models
from .usuario import Usuario

class Factura(models.Model):
    id = models.AutoField(primary_key=True)
    cedula = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    tarifa = models.IntegerField(default=0)
    total = models.IntegerField(default=0)
    tiempo = models.DateTimeField()