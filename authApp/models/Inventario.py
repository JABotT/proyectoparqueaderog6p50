from django.db import models
from .Administrador import Administrador
from .usuario import Usuario

class Inventario(models.Model):
    espacio = models.IntegerField(primary_key=True)
    id = models.ForeignKey(Administrador, on_delete=models.CASCADE)
    asignacion = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha = models.CharField(max_length = 20)
    tipo = models.CharField( max_length = 20)
    estado = models.EmailField(max_length = 50)
    