from django.db import models
from .usuario import Usuario

class Vehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    placa = models.CharField(max_length = 10)
    propietario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    tipo = models.CharField(max_length = 8)
    modelo = models.CharField(max_length = 8)
    
