from django.contrib import admin

from .models.usuario import Usuario
from .models.Factura import Factura
from .models.Vehiculo import Vehiculo
from .models.Administrador import Administrador
from .models.Inventario import Inventario

admin.site.register(Usuario)
admin.site.register(Factura)
admin.site.register(Vehiculo)
admin.site.register(Administrador)
admin.site.register(Inventario)

# Register your models here.
