"""authProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from authApp import views

urlpatterns = [

    path('administrador/', views.AdministradorCreateView),
    path('factura/', views.factura_api_view),
    path('factura/<int:pk>', views.factura_detail_view),
    path('inventario/', views.inventario_api_view),
    path('inventario/<int:pk>', views.inventario_detail_view),
    path('usuario/', views.usuario_api_view),
    path('usuario/<int:pk>', views.usuario_detail_view),
    path('vehiculo/', views.vehiculo_api_view),
    path('vehiculo/<int:pk>', views.vehiculo_detail_view),


]
